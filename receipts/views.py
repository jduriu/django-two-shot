from django.shortcuts import redirect
from django.urls import reverse_lazy

# from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from django.contrib.auth.mixins import LoginRequiredMixin

# from django.views.decorators.http import require_http_methods
# from django.db import IntegrityError

# from recipes.forms import RatingForm

from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/create.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect(reverse_lazy("home"))


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class CategoryCreateView(CreateView):
    model = ExpenseCategory
    fields = ["name"]
    template_name = "categories/create.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect(reverse_lazy("list_categories"))


class AccountCreateView(CreateView):
    model = Account
    fields = ["name", "number"]
    template_name = "accounts/create.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect(reverse_lazy("list_accounts"))
